package dev.asplundh.controllers;

import dev.asplundh.models.CarModel;
import dev.asplundh.services.CarModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.javalin.http.Context;

public class CarModelController {

    Logger logger = LoggerFactory.getLogger(CarModelController.class);

    // create a CarModelService Object
    private CarModelService service = new CarModelService();


    // This method retrieves all car models from the database
    public void handleGetCarModelRequest(Context ctx) {
        ctx.json(service.getAll());                             // call the service method for getAll()
        logger.info("Getting all car models");                  // Log if successful
        ctx.status(200);                                        // return normal status code to client
    }

    public void handleGetCarModelByNameRequest(Context ctx) {
        String name = ctx.formParam("carName");
        ctx.json(service.getCarByName(name));
        ctx.status(200);
    }

    // This method assigns the values from the given JSON
    // to a CarModel object to be passed into the service's
    // "add" method
    public void handlePostCarModelRequest(Context ctx) {
        String makeId = ctx.formParam("makeId");
        String makeName = ctx.formParam("makeName");
        String region = ctx.formParam("region");
        String modelName = ctx.formParam("modelName");
        String engine = ctx.formParam("engine");
        String zeroToSixty = ctx.formParam("zeroToSixty");
        String topSpeed = ctx.formParam("makeId");
        String mpg = ctx.formParam("mpg");
        String price = ctx.formParam("price");
        service.add(makeId, makeName, region, modelName, engine, zeroToSixty, topSpeed, mpg, price);                                       // call the service method for add();
        logger.info("data has been added to database");         // Log if successful
        ctx.status(201);                                        // return created status code to client
    }


    // This method takes in the model_id of the car
    // in the database to be updated.  It replaces
    // the entry with the name given from the client
    public void handlePutCarModelRequestById(Context ctx) {
        String oldName = ctx.formParam("oldName");                 // model_id given from client
        String newName =  ctx.formParam("newName");            // model name given from client
        service.put(oldName, newName);                                  // call the service method for put()
        logger.info("Entry has been updated");                  // Log if successful
        ctx.status(201);                                        // return created status code to client
    }

    public void handleDeleteCarModelRequestByName(Context ctx) {
        String name = ctx.formParam("carName");
        service.delete(name);
        logger.info("Entry has been deleted");
        ctx.status(200);
    }

    // This method takes in two names of cars from
    // the client to "race" them by their top speed.
    // It calls the service method which passes the
    // name parameters to the database to be retrieved.
    // Those are then returned to the service method to
    // determine which car is faster.
    public void handleGetRaceByTopSpeed(Context ctx) {
        String name1 = ctx.formParam("carOne");             // Grab car1's name
        String name2 = ctx.formParam("carTwo");             // Grab car2's name
        ctx.json(service.raceTopSpeed(name1, name2));           // pass them to the service method to race them
        logger.info("Cars successfully raced.");                // Log if cars raced
        ctx.status(200);                                        // return normal status code to client


    }

    // Exact method as above, except races cars by their
    // 0-60 acceleration.
    public void handleGetRaceByAcceleration(Context ctx) {
        String name1 = ctx.formParam("carOne");
        String name2 = ctx.formParam("carTwo");
        ctx.json(service.raceAcceleration(name1, name2));
        ctx.status(200);
    }

}
