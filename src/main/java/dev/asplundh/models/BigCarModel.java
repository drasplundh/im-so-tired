package dev.asplundh.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;

@Entity
public class BigCarModel {
    @Id
    private int id;
    private String region;
    private String make;
    private String model;
    private String engine;
    private int topSpeed;
    private double zeroToSixty;
    private double mpg;
    private double price;

    public BigCarModel() {
        //super();
    }

    public BigCarModel(String region, String make, String model, String engine, int topSpeed, double zeroToSixty, double mpg, double price) {
        this.id = id;
        this.region = region;
        this.make = make;
        this.model = model;
        this.engine = engine;
        this.topSpeed = topSpeed;
        this.zeroToSixty = zeroToSixty;
        this.mpg = mpg;
        this.price = price;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    public double getZeroToSixty() {
        return zeroToSixty;
    }

    public void setZeroToSixty(double zeroToSixy) {
        this.zeroToSixty = zeroToSixy;
    }

    public double getMpg() {
        return mpg;
    }

    public void setMpg(double mpg) {
        this.mpg = mpg;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
