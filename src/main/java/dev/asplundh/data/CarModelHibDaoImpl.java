package dev.asplundh.data;

import dev.asplundh.models.BigCarModel;
import dev.asplundh.models.CarModel;
import dev.asplundh.models.CarSpeed;
import dev.asplundh.models.MakeModel;
import dev.asplundh.util.ConnectionUtil;
import dev.asplundh.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class CarModelHibDaoImpl implements CarModelDao {

    @Override
    public List<CarModel> getAllModels() {
        try (Session s = HibernateUtil.getSession()) {
            List<CarModel> models = s.createQuery("from CarModel", CarModel.class).list();
            System.out.println(models);
            return models;

        }
    }

    public CarModel getModelByName(String name) {
        try (Session s = HibernateUtil.getSession()) {
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<CarModel> cr = cb.createQuery(CarModel.class);
            Root<CarModel> root = cr.from(CarModel.class);
            cr.select(root).where(cb.like(root.get("name"), name));

            Query<CarModel> query = s.createQuery(cr);
            CarModel car = query.getSingleResult();
            System.out.println("Your car's name is: " + car.getName());
            return car;
        }

    }

    @Override
    public void addNewCarModel(String makeId, String makeName, String region, String modelName,
                               String engine, String topSpeed,
                               String zeroToSixty, String mpg, String price) {
                try (Connection connection = ConnectionUtil.getConnection()) {
                    PreparedStatement preparedStatement =
                            connection.prepareStatement(
                                    "insert into model values (default, (?), (?), (?), (?), (?), (?), (?), null");
                    preparedStatement.setInt(1, Integer.parseInt(makeId));
                    preparedStatement.setString(2, modelName);
                    preparedStatement.setString(3, engine);
                    preparedStatement.setDouble(4, Double.parseDouble(topSpeed));
                    preparedStatement.setDouble(5, Double.parseDouble(zeroToSixty));
                    preparedStatement.setDouble(6, Double.parseDouble(mpg));
                    preparedStatement.setDouble(7, Double.parseDouble(price));
                    preparedStatement.executeQuery();
                } catch (SQLException e) {
                    e.printStackTrace();
                }



//        try (Session s = HibernateUtil.getSession()) {
//            Transaction tx = s.beginTransaction();
//            Query<CarModel> query = s.createNativeQuery("insert into model values (default, :makeId, :modelName, :engine, :topSpeed, :zeroToSixty, :mpg, :price, null)");
//            query.setParameter("makeId", makeId);
//            query.setParameter("engine", engine);
//            query.setParameter("modelName", modelName);
//            query.setParameter("topSpeed", topSpeed);
//            query.setParameter("zeroToSixty", zeroToSixty);
//            query.setParameter("mpg", mpg);
//            query.setParameter("price", price);
//            query.executeUpdate();
//            tx.commit();
        }

    @Override
    public List<CarSpeed> raceByTopSpeed(String name1, String name2) {
        return null;
    }

    @Override
    public List<CarSpeed> raceByAcceleration(String name1, String name2) {
        return null;
    }

    @Override
    public void putCarModel(String oldName, String newName) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
//            MakeModel deleteMake = new MakeModel(name);
            Query<CarModel> query = s.createQuery("update CarModel set name = :newName where name = :oldName");
            query.setParameter("newName", newName);
            query.setParameter("oldName", oldName);
            query.executeUpdate();
            tx.commit();

        }
    }


//    public void deleteCarModel(String name) {
//        System.out.println("name = " + name);
//        try (Session s = HibernateUtil.getSession()) {
//            Transaction tx = s.beginTransaction();
//            CarModel deleteCar = new CarModel(name);
//            s.save(deleteCar);
//            s.delete(deleteCar);
//            tx.commit();
//        }
    @Override
    public void deleteCarModel(String name) {
        System.out.println("name = " + name);
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
//            MakeModel deleteMake = new MakeModel(name);
            Query<CarModel> query = s.createQuery("delete from CarModel where name = :name");
            query.setParameter("name", name);
            query.executeUpdate();
            tx.commit();
        }
    }
}
