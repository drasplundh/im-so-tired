package dev.asplundh.data;

import dev.asplundh.models.UserModel;


public interface UserLoginDao {
    public UserModel getLoginCredentials(UserModel loginUser);
}
