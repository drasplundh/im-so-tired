package dev.asplundh.util;

import dev.asplundh.models.BigCarModel;
import dev.asplundh.models.CarModel;
import dev.asplundh.models.MakeModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;


public class HibernateUtil {

    private static SessionFactory sessionFactory;
    private static SessionFactory getSessionFactory() {

        if(sessionFactory==null) {
            Configuration configuration = new Configuration();
            Properties settings = new Properties();
            settings.put(Environment.URL, "jdbc:postgresql://drasplundh-java-azure-training.postgres.database.azure.com:5432/postgres");
            settings.put(Environment.USER, "drasplundh@drasplundh-java-azure-training");
            settings.put(Environment.PASS, "GumChunks52!");

            // for postgres
            settings.put(Environment.DRIVER, "org.postgresql.Driver");
            settings.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQLDialect");

            settings.put(Environment.HBM2DDL_AUTO, "update");
            settings.put(Environment.SHOW_SQL, "true");

            configuration.setProperties(settings);


            configuration.addAnnotatedClass(CarModel.class);
            configuration.addAnnotatedClass(MakeModel.class);

            sessionFactory = configuration.buildSessionFactory();

        }
        return sessionFactory;
    }

    public static Session getSession() {
        return getSessionFactory().openSession();
    }
}