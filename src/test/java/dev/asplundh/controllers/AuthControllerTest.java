package dev.asplundh.controllers;

import dev.asplundh.models.UserModel;
import dev.asplundh.services.UserService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class AuthControllerTest {


    @InjectMocks
    AuthController authController;

//    @Mock
//    UserService loginUser;
//
//    @Mock
//    TokenManager manager;

    @BeforeEach
    public void initMocks(){
    MockitoAnnotations.initMocks(this);
}

    @Test
    public void loginSuccessfulTest() {
        String user = "duncan";
        String pass = "password";
        UserModel userLogin = new UserModel(user, pass);
        Context ctx = mock(Context.class);
        UserService loginUser = mock(UserService.class);
        TokenManager manager = mock(TokenManager.class);
        when(ctx.formParam("username")).thenReturn("duncan");
        when(ctx.formParam("password")).thenReturn("password");
        when(loginUser.getCredentials((userLogin))).thenReturn(true);
        when(manager.issueToken("duncan")).thenReturn("some-token");
        authController.login(ctx);
        verify(ctx).header("Authorization", "some-token");
    }

//    public void loginFailureTest() {
//        String user = "duncan";
//        String pass = "inccorectpassword";
//        UserModel userLogin = new UserModel(user, pass);
//        Context ctx = mock(Context.class);
//        UserService loginUser = mock(UserService.class);
//        when(ctx.formParam("username")).thenReturn("duncan");
//        when(ctx.formParam("password")).thenReturn("incorrectpassword");
//        when(loginUser.getCredentials((userLogin))).thenReturn(false);
//    }
}
