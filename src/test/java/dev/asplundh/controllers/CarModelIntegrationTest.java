package dev.asplundh.controllers;

import dev.asplundh.Driver;
import dev.asplundh.models.CarModel;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CarModelIntegrationTest {

    private static Driver app = new Driver();

    /*
    @BeforeAll
    public static void startService() {
        app.start(7000);
    }

    @AfterAll
    public static void stopService() {
        app.stop();
    }
    */


    @Test
    public void testGetAllCarModels() {
        HttpResponse<List<CarModel>> response = Unirest.get("http://localhost:7000/carmodels")
                .asObject(new GenericType<List<CarModel>>() {
                });
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertFalse(response.getBody().isEmpty())
        );
    }

    @Test
    public void testPostCarModel() {
        HttpResponse response = Unirest.post("http://localhost:7000/carmodels").body("{\"modelId\": 9,\"makeId\": 4,\"name\": \"DummyCar\"}").asEmpty();

        assertAll(
                () ->  assertEquals(201, response.getStatus())
        );

    }


    //carId carName params
    @Test
    public void testPutCarModel() {
        HttpResponse response = Unirest.put("http://localhost:7000/carmodels")
                .field("carId", "1")
                .field("carName", "Tuscan").asEmpty();
        /*
                .queryString("carId", "1")
                .queryString("carName", "Tuscan")
                .asEmpty();

         */
        assertAll(
                () -> assertEquals(201, response.getStatus())
        );
    }

    @Test
    public void testDeleteCarModel() {
        HttpResponse response = Unirest.delete("http://localhost:7000/carmodels").field("name", "DummyCar").asEmpty();

        assertAll(
                () -> assertEquals(200, response.getStatus())
        );
    }

}
