//package dev.asplundh.services;
//import dev.asplundh.data.CarModelDaoImpl;
//import dev.asplundh.models.CarModel;
//import dev.asplundh.models.CarSpeed;
//import dev.asplundh.services.CarModelService;
//import io.javalin.http.Context;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.when;
//
//public class CarModelServiceTest {
//
//
//
//    @InjectMocks
//    CarModelService service;
//
//    @Mock
//    CarModelDaoImpl carModelDao;
//
//    @BeforeEach
//    public void initMocks(){
//        MockitoAnnotations.initMocks(this);
//    }
//
//
//    @Test
//    public void testRaceByTopSpeedTie() {
//        List<CarSpeed> cars = new ArrayList<>();
//        CarSpeed car1 = new CarSpeed("Diablo", 200);
//        CarSpeed car2 = new CarSpeed("Sagaris", 200);
//        cars.add(car1);
//        cars.add(car2);
//        when(carModelDao.raceByTopSpeed("Diablo", "Sagaris")).thenReturn(cars);
//        assertEquals("It's a tie!", service.raceTopSpeed("Diablo", "Sagaris"));
//    }
//
//
//    @Test
//    public void testRaceByTopSpeedTrue() {
//        List<CarSpeed> cars = new ArrayList<>();
//        CarSpeed car1 = new CarSpeed("Sagaris", 185);
//        CarSpeed car2 = new CarSpeed("Diablo", 200);
//        cars.add(car1);
//        cars.add(car2);
//        when(carModelDao.raceByTopSpeed("Diablo", "Sagaris")).thenReturn(cars);
//        assertEquals("Diablo with a top Speed of 200 is faster", service.raceTopSpeed("Diablo", "Sagaris"));
//    }
//
//
//    @Test
//    public void testRaceByTopSpeedFalse() {
//        List<CarSpeed> cars = new ArrayList<>();
//        CarSpeed car1 = new CarSpeed("Bronco", 135);
//        CarSpeed car2 = new CarSpeed("Sagaris", 185);
//        cars.add(car1);
//        cars.add(car2);
//        when(carModelDao.raceByTopSpeed("Bronco", "Sagaris")).thenReturn(cars);
//        assertEquals("Sagaris with a top Speed of 185 is faster", service.raceTopSpeed("Bronco", "Sagaris"));
//    }
//
//
//}
